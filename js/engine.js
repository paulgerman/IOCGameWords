class GameEngine {
    constructor() {

        //initial
        this.nScore = 0;
        this.nWordsCorrect = 0;
        this.nLevel = 1;
        this.arrWordIntervals = [];
        this.arrWordTimeouts = [];


        //config this
        this.nTime = 3;
        this.nLivesLeft = 5;
        this.nWordsAfterGoingToNextLevel = 5;
        this.nTotalTime = 0;
        
        // first element (getItem("0")) on the localStorage is the ID for the next element
        this.nCrtID = localStorage.getItem("0");
        //console.log("first crtID:" + this.nCrtID);
        if (this.nCrtID == null)
        {
        	this.nCrtID = 1;
        	localStorage.setItem("0", "1")
        }
        else
        	this.nCrtID = parseInt(this.nCrtID);


        //get the elements from the DOM
        this.elScore = document.getElementById("score");
        this.elTimer = document.getElementById("timer");
        this.elLevel = document.getElementById("level");
        this.elInput = document.getElementById("input");
        this.elLives = document.getElementById("lives");
        this.elWord = document.getElementById("word");


        this.updateScore();
        this.updateTimer();
        this.updateLevel();
        this.updateLives();


        this.showLeadboard();
    }

    startGame() {
        console.log("Game started");

        document.getElementById("start").style.display = "none";
        document.getElementById("game-end").style.display = "none";
        document.getElementById("leadboard").style.display = "none";
        document.getElementById("hallOfFame").style.display = "none";

        this.strWord = this.generateWord();
        this.updateWord();

        //timer interval
        this.intervalTimer = setInterval(() => {
            this.nTime -= 1;
            this.nTotalTime += 1;
            this.updateTimer();
        }, 1000);

        document.getElementById("game-panel").style.display = "block";
        this.elInput.focus();

        this.playEffect("sounds/correct.mp3", "correct");
    }

    updateScore() {
        this.elScore.innerText = this.nScore;
    }

    updateTimer() {
        this.elTimer.innerText = this.nTime;
        if (this.nTime <= 0) {
        	this.nTime = 0;
            this.endGame();
            return;
        }
    }

    updateLevel() {
        this.elLevel.innerText = this.nLevel;
    }

    updateWord() {
        while (this.elWord.firstChild) {
            this.elWord.removeChild(this.elWord.firstChild);
        }


        let updatedStrWord = "";
        for (let ch in this.strWord) {
            let elDiv = document.createElement("div");
			if((this.randomIntFromInterval(0,1) || this.strWord[ch]=="i") && this.strWord[ch]!="l")
				elDiv.innerText = (this.strWord[ch]).toLowerCase();
			else
				elDiv.innerText = (this.strWord[ch]).toUpperCase();

			updatedStrWord += elDiv.innerText;	

			
			if(this.strWord[ch] == " ")
			{
				elDiv.style.width = "15px";
			}
            elDiv.style.display = "inline-block";
            elDiv.style.marginRight = "10px";
            this.elWord.appendChild(elDiv);
        }

        this.strWord = updatedStrWord;

        this.animateWord();
        this.elInput.value = "";
    }

    updateLives() {
        this.elLives.innerText = this.nLivesLeft;
        if (this.nLivesLeft <= 0) {
            this.endGame();
            return false;
        }
        return true;
    }

    endGame() {	

        while (this.elWord.firstChild)
            this.elWord.removeChild(this.elWord.firstChild);
        console.log("Game ended!");
        clearInterval(this.intervalTimer);

        document.getElementById("game-panel").style.display = "none";
        document.getElementById("game-end").style.display = "block";
        this.elInput.value = "";
        document.getElementById("start").style.display = "inline";
        for (let i in this.arrWordIntervals)
            clearInterval(this.arrWordIntervals[i]);
        for (let i in this.arrWordTimeouts)
            clearTimeout(this.arrWordTimeouts[i]);


        // leadboard
        document.getElementById("hallOfFame").style.display = "block";
        document.getElementById("leadboard").style.display = "block";
        
        let isInHallOfFame = false;
        if ( this.nScore != 0 )
        {
	        if ( localStorage.length < 8 )
	        {
	        	//console.log("crtID:" + this.nCrtID);
	        	document.getElementById("backgroundGame").muted = true;
	        	this.playEffect("sounds/hallOfFame.mp3", "effect");
	        	$('#popup-name').modal('show');
	        	isInHallOfFame = true;
	        }
	        else
	        {
	        	let minKey = "";
	        	let minScore = 9999999;

		    	for (let i = 1; i < localStorage.length; i++){

		    		let args = localStorage.getItem(localStorage.key(i)).split(" ");

		    		if ( parseInt(args[1]) < minScore )
		    		{
		    			minScore = parseInt(args[1]);
		    			minKey = localStorage.key(i);
		    		}
		    	}

		    	//console.log("min:" + minScore + " crt:" + this.nScore);
		    	if (minScore < this.nScore )
		    	{
		    		//console.log("crtID:" + this.nCrtID);
		    		document.getElementById("backgroundGame").muted = true;
		    		this.playEffect("sounds/hallOfFame.mp3", "effect");
		    		$('#popup-name').modal('show');
		    		isInHallOfFame = true;
		    	}
	        }
    	}

    	if (isInHallOfFame == false)
    		this.playEffect("sounds/wrong.mp3", "wrong");
    }

    updateLeadboard(){
    	let name = document.getElementById("inputName").value.trim();
    	if ( name == "" )
    	{
    		document.getElementById("inputName").value = "";
    		document.getElementById("inputName").focus();
    		return;
    	}

        $("#popup-name").modal('hide');	

        if ( localStorage.length < 8 )
        {
        	localStorage.setItem("" + this.nCrtID, "" + name + " " + this.nScore + " " + this.nTotalTime);
        }
        else
        {
        	let minKey = "";
        	let minScore = 9999999;

	    	for (let i = 1; i < localStorage.length; i++){

	    		let args = localStorage.getItem(localStorage.key(i)).split(" ");

	    		if ( parseInt(args[1]) < minScore )
	    		{
	    			minScore = parseInt(args[1]);
	    			minKey = localStorage.key(i);
	    		}
	    	}	

	    	//console.log("minKey:" + minKey);
	    	localStorage.removeItem(minKey);
	    	localStorage.setItem("" + this.nCrtID, "" + name + " " + this.nScore + " " + this.nTotalTime);
        }

        this.nCrtID += 1;
        localStorage.setItem("0", this.nCrtID);

        this.showLeadboard();
    }

    showLeadboard(){
    	let datas = new Array();
    	for (let i = 1; i < localStorage.length; i++){

    		let args = localStorage.getItem(localStorage.key(i)).split(" ");

    		datas.push({name:args[0], score:args[1], time:args[2]});
    	}

    	datas.sort(function(a, b){return (parseInt(b.score) > parseInt(a.score)) ? 1 : ((parseInt(a.score) > parseInt(b.score)) ? -1 : 0);});

		var tableRef = document.getElementById('tableBody');
		$("#tableBody").empty();

    	for (let i = 0; i < datas.length; i++){

    		// Insert a row in the table at row index 0
    		var newRow   = tableRef.insertRow(tableRef.rows.length);

    		// Insert a cell in the row at index 0
  			var nrCell  = newRow.insertCell(0);

  			// Append a text node to the cell
  			var nrText  = document.createTextNode("" + (i+1));
  			nrCell.appendChild(nrText);


  			var nameCell = newRow.insertCell(1);
  			var nameText = document.createTextNode("" + datas[i].name);
  			nameCell.appendChild(nameText);

			var scoreCell = newRow.insertCell(2);
  			var scoreText = document.createTextNode("" + datas[i].score);
  			scoreCell.appendChild(scoreText);  			

  			var timeCell = newRow.insertCell(3);
  			var timeText = document.createTextNode("" + datas[i].time + " seconds");
  			timeCell.appendChild(timeText);
    	}
    }

    nextWord() {
        //ignore empty input
        let strValue = this.elInput.value;
        if (strValue.trim() == "")
            return;

        //correct input
		console.log(strValue.trim().toLowerCase(), this.strWord);
		//console.log(strValue.trim());
		//console.log(this.strWord);
        if (strValue.trim().toLowerCase() == this.strWord.toLowerCase()) {

        	this.playEffect("sounds/correct.mp3", "correct");

            this.nScore += this.strWord.length * 10;
            this.updateScore();
            this.nWordsCorrect++;
			this.nTime += 7;
            this.updateTimer();
        }
        //wrong input
        else {

        	this.playEffect("sounds/wrong.mp3", "wrong");

			this.nTime -= 3;
            this.updateTimer();
			this.nScore -= this.strWord.length * 3;
			if(this.nScore < 0){
				this.nScore = 0;
			}
            this.updateScore();
            this.nLivesLeft--;
            document.getElementsByTagName("BODY")[0].style.backgroundColor = "red";
            setTimeout(() => {
                document.getElementsByTagName("BODY")[0].style.backgroundColor = "white";
            }, 300);
            if (!this.updateLives())
                return;
        }

        //go to next level
        if (this.nWordsCorrect == this.nWordsAfterGoingToNextLevel) {
			this.nTime -= 3;
            this.updateTimer();
            /*if (this.nLevel == nLevelsNum) {
                this.endGame();
                return;
            }*/
			if (this.nLevel != nLevelsNum){
				this.nWordsCorrect = 0;
				this.nLevel++;
				this.updateLevel();
			}
        }
        this.strWord = this.generateWord();
        this.updateWord();
    }

    generateWord() {
        return arrLevels[this.nLevel][this.randomIntFromInterval(0, arrLevels[this.nLevel].length - 1)];
    }

    randomIntFromInterval(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    animateWord() {
        //clear time events
        for (let i in this.arrWordIntervals)
            clearInterval(this.arrWordIntervals[i]);
        for (let i in this.arrWordTimeouts)
            clearTimeout(this.arrWordTimeouts[i]);

        this.arrWordIntervals = [];
        this.arrWordTimeouts = [];

        let arrChildren = this.elWord.children;
        for (let i in arrChildren) {
            if (arrChildren.hasOwnProperty(i)) {
                let nTime = (1.2 - ((this.nLevel - 1) / (nLevelsNum - 1)) * 0.9) * this.randomIntFromInterval(90, 100) / 100;
                arrChildren[i].style.transition = "all " + nTime + "s ease";

                this.arrWordTimeouts.push(setTimeout(() => {
                    //Rotation animation
                    let nRotationDegree;
                    if (this.randomIntFromInterval(1, 2) == 1)
                        nRotationDegree = 30 + 20 * (this.nLevel - 1);
                    else
                        nRotationDegree = -30 + 20 * (this.nLevel - 1);

                    let fnRotateTo = () => {
                        arrChildren[i].style.transform = "rotate(" + nRotationDegree + "deg)";
                    };

                    let fnRotateInitial = () => {
                        arrChildren[i].style.transform = "rotate(0deg)";
                    };

                    this.arrWordIntervals.push(setInterval(fnRotateTo, nTime * 2 * 1000));
                    this.arrWordTimeouts.push(setTimeout(() => {
                        this.arrWordIntervals.push(setInterval(fnRotateInitial, nTime * 2 * 1000));
                        fnRotateInitial();
                    }, nTime * 1000));
                    fnRotateTo();


                    //Size animation
                    let fnSizeTo = () => {
                        arrChildren[i].style.fontSize = "50px";
                    };

                    let fnSizeInitial = () => {
                        arrChildren[i].style.fontSize = "40px";
                    };
                    this.arrWordIntervals.push(setInterval(fnSizeTo, nTime * 2 * 1000));
                    this.arrWordTimeouts.push(setTimeout(() => {
                        this.arrWordIntervals.push(setInterval(fnSizeInitial, nTime * 2 * 1000));
                        fnSizeInitial();
                    }, nTime * 1000));
                    fnSizeTo();
                }, 5));
            }
        }
    }

    playEffect(source, id){
    	console.log("wrong");
    	let son = document.createElement("audio");
		son.setAttribute("src", source);
		son.setAttribute("hidden","true");
		son.setAttribute("id", id)
		son.volume = 1;
		document.body.appendChild(son);
		son.play();
    }
}